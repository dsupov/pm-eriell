@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Информационная панель ДУП') }}</div>

                    <div class="card-body">
                        <p>Все гуд</p>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Example select</label>
                            <select class="my-select" id="exampleFormControlSelect1">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>

                            <select class="selectpicker" data-live-search="true">
                                <option data-tokens="ketchup mustard">Hot Dog, Fries and a Soda</option>
                                <option data-tokens="mustard">Burger, Shake and a Smile</option>
                                <option data-tokens="frosting">Sugar, Spice and all things nice</option>
                            </select>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
